#!/bin/bash

current_version=$(curl https://factorio.com/api/latest-releases | jq -r ".experimental.headless")

installed_version=$(/opt/factorio/bin/x64/factorio --version | grep Version | awk '{print $2}')

if [ "$current_version" == "$installed_version" ]; then
	echo "Versions still match"
	echo "UPDATE STATUS: no update"
else
	echo "Versions differ. Updating server"
	wget -O /opt/factorio.tar.xz https://factorio.com/get-download/latest/headless/linux64
  tar xJvf "/opt/factorio.tar.xz"
	rconclt factorio "Update applied. Server will restart in 10 seconds"
	for num in {10..1}
	do
		rconclt factorio $num
		sleep 1
	done
	rconclt factorio "Goodbye!"
	echo "UPDATE STATUS: updated"
fi
