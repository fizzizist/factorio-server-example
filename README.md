# Factorio Server Example

This is an example of a Factorio headless setup using Docker Compose.

### Notes
* This is just an example, and is pretty experimental. I am sure it contains some bad practices.
* The factorio version being pulled in the Dockerfile is the experimental Beta version. If you want just the latest stable version then you need to change the URL in the Dockerfile.
* If you're spinning this up on a home server, you're behind a router, and you want your server to be available on the internet, you need to port-forward 34197/udp. Be careful though, exposing ports to the internet is always a risk.

## Initial Setup

Spin up the server initially to generate the world and settings files.
```bash
docker compose up
```
ctrl+C to get out of that.

Then go edit your new settings files at `./settings/`.

If you want to be an admin, put your username in `settings/server-adminlist.json`

Once you have all your settings done, you should be able to fire up your server again with 
```bash
docker compose up -d
```

Now log into Factorio and go to Multiplayer to find your server.

## Updater Setup

If you want to ensure that your server is up to date. You can schedule a cron job to run the `updater-cron.sh` script once a day. This will check the current installed version with the latest one and update if necessary and then restart the server if it needs it. Don't forget to pass in the path to local git repository into the script.

For example:
```
0 3 * * * /home/username/factorio-server-example/updater-cron.sh /home/username/factorio-server-example
```
This would run the updater every day at 3AM.
