cd $1

update_status=$(docker compose run updater /opt/factorio-updater.sh | grep "UPDATE STATUS")

if [ "$update_status" == "UPDATE STATUS: updated" ]; then
	docker compose restart factorio
fi
