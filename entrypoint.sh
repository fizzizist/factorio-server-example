#!/bin/bash

if ! [ -f /opt/savegames/world.zip ]; then
	echo "No savegame found. Creating new world"
	mkdir -p /opt/savegames
	/opt/factorio/bin/x64/factorio --create /opt/savegames/world.zip
fi

if ! [ -f /opt/settings/server-settings.json ]; then
	echo "No settings file found. Generating new one"
	mkdir -p /opt/settings
	cp /opt/factorio/data/server-settings.example.json /opt/settings/server-settings.json
fi

if ! [ -f /opt/settings/server-adminlist.json ]; then
	echo "No adminlist file found. Generating new one"
	echo [] > settings/server-adminlist.json
fi

/opt/factorio/bin/x64/factorio --start-server /opt/savegames/world.zip --server-settings /opt/settings/server-settings.json --server-adminlist /opt/settings/server-adminlist.json --rcon-port 25575 --rcon-password "rcon-pass"
