FROM debian:bookworm-slim as factorio

WORKDIR /opt

RUN apt update \
  && apt install -y wget xz-utils \
	&& wget -O /opt/factorio.tar.xz https://factorio.com/get-download/latest/headless/linux64 \
  && tar xJvf "/opt/factorio.tar.xz"

COPY entrypoint.sh /opt/entrypoint.sh

WORKDIR /opt/factorio

ENTRYPOINT /opt/entrypoint.sh

FROM debian:bookworm-slim as updater

WORKDIR /opt

RUN apt update && apt install -y cron xz-utils curl wget jq rcon

COPY factorio-updater.sh /opt/factorio-updater.sh
COPY rcon.conf /etc/rcon.conf

RUN chmod +x /opt/factorio-updater.sh
